function sortObject(obj) {
    var arr = [];
    for (var prop in obj) {
        if (obj.hasOwnProperty(prop)) {
            arr.push({
                'key': prop,
                'value': obj[prop]
            });
        }
    }
    arr.sort(function(a, b) { return b.value - a.value; });
    //arr.sort(function(a, b) { a.value.toLowerCase().localeCompare(b.value.toLowerCase()); }); //use this to sort as strings
    return arr; // returns array
}

var net = require('net'),
	arpad = require('arpad');

elo = new arpad();

scores = {
	"micro10_forked":1000,
	"micro10_original":1000,
	"micro9":1000,
	"micro11":1000,
	"micro8":1000,
	"microoptimized10":1000,
	"micro5":1000,
	"micro4":1000
};

var server = net.createServer(function(c) {
	console.log('client connected');
	c.on('end', function() {
		console.log('client disconnected');
	});
	c.on('data', function(data) {
		console.log(">", data.toString('utf8'));
		data = data.toString('utf8').split('\r\n')[0].toLowerCase().split(' ');
		switch (data[0]) {
			case "help":
				c.write("add <player>: adds a new player with score 1000 (eg. \"add GravityCoders\").\r\n");
				c.write("leaderboard: displays the current leaderboard.\r\n");
				c.write("match <player1> <win/tie/lose> <player2>: records the result of a match (eg. \"match GravityCoders win Fermi\").\r\n");
				c.write("quit: closes the connection\r\n");
				break;
			case "add":
				if (data[1] in scores) {
					c.write("Player " + data[1] + " already registered\r\n");
				} else {
					scores[data[1]] = 1000;
					c.write("Player " + data[1] + " added, score 1000\r\n");
				}
				break;
			case "leaderboard":
				leaderboard = sortObject(scores);
				console.log(JSON.stringify(leaderboard));
				for (place in leaderboard) {
					c.write(place + '\t' + leaderboard[place].key + '\t' + leaderboard[place].value + '\r\n');
				}
				break;
			case "match":
				if ((data[1] in scores) && (data[3] in scores)) {
					switch (data[2]) {
						case "win":
							scores[data[1]] = elo.newRatingIfWon(scores[data[1]], scores[data[3]]);
							scores[data[3]] = elo.newRatingIfLost(scores[data[3]], scores[data[1]]);
							break;
						case "tie":
							scores[data[1]] = elo.newRatingIfTied(scores[data[1]], scores[data[3]]);
							scores[data[3]] = elo.newRatingIfTied(scores[data[3]], scores[data[1]]);
							break;
						case "lose":
							scores[data[1]] = elo.newRatingIfLost(scores[data[1]], scores[data[3]]);
							scores[data[3]] = elo.newRatingIfWon(scores[data[3]], scores[data[1]]);
							break;
					}
					c.write("Match recorded!\r\n");
				} else {
					c.write("Team " + data[1] + " or " + data[3] + " not found!\r\n");
				}
				break;
			case "quit":
				c.end("\r\n");
				break;
			default:
				c.write("Command not recognized, try \"help\" for a list of commands\r\n");
				break;
		}
	});
});

server.listen(8124, function() {
	console.log('server bound');
});
